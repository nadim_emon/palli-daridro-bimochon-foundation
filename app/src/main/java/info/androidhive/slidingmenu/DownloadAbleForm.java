package info.androidhive.slidingmenu;

import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;

public class DownloadAbleForm extends Fragment {
    MessangerDatabaseAdapter dbadapter;
    TextView et;

    public DownloadAbleForm() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_download_able_form, container, false);

        dbadapter = new MessangerDatabaseAdapter(getActivity());
        dbadapter.open();



        WebView webView = (WebView) rootView.findViewById(R.id.DownloadAbleFormInformation);
        String customHtml = makinglist("DownloadAbleFormInformation");
        webView.loadData(customHtml, "text/html; charset=UTF-8", null);

        //webView.loadData(s, "text/html; charset=UTF-8", null);

        return rootView;
    }

    private String makinglist(String appcode) {
        SQLiteDatabase db = dbadapter.getDSqLiteDatabase();

        Cursor cursor = db.query("PDBF", null, " APPCODE=?", new String[]{appcode}, null, null, null);

        if (cursor.getCount() < 1) {

            cursor.close();
            return "NOT EXISTS";
        }

        cursor.moveToFirst();
        String content = cursor.getString(cursor.getColumnIndex("CONTENT"));
        cursor.close();

        return content;

    }
}

