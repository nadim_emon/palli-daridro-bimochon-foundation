package info.androidhive.slidingmenu;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class ObeseTreatmentFragment extends Fragment {
	
	public ObeseTreatmentFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_obese_treatment, container, false);

        WebView webView = (WebView)rootView.findViewById(R.id.home);
        String customHtml = MainActivity.content[0];
        webView.loadData(customHtml, "text/html", "UTF-8");

        return rootView;
    }
}
