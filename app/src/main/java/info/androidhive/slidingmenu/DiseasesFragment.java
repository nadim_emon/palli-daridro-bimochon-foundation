package info.androidhive.slidingmenu;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class DiseasesFragment extends Fragment {
	
	public DiseasesFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_diseases, container, false);

        String web="<html><body><img src='@drawable/map.jpg' alt='map'></body></head>";
        WebView webView = (WebView) rootView.findViewById(R.id.DoptorInformation);
        //String customHtml = makinglist("DoptorInformation");
        webView.loadData(web, "text/html; charset=UTF-8", null);

         
        return rootView;
    }
}
