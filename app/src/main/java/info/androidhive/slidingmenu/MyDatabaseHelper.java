package info.androidhive.slidingmenu;




import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class MyDatabaseHelper extends SQLiteOpenHelper{

	public MyDatabaseHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(MessangerDatabaseAdapter.TABLE_SEND);

 	  
 	   
 	  ContentValues values1=new ContentValues();
 	  values1.put("APPCODE", "DoptorInformation");
 	  values1.put("CONTENT","<html><body><p>পল্লী দারিদ্রবিমোচনের লক্ষ্যে জেলা ও উপজেলা পর্যায়ে পল্লী দারিদ্র বিমোচন ফাউন্ডেশন (পিডিবিএফ) স্থাপন করা হয়েছে। ক্ষুদ্রঋণ থেকে শুরু করে নারীর ক্ষমতায়ন, শিক্ষা সম্প্রসারণ, কৃষি বিষয়ক নানা কার্যক্রমের মাধ্যমে পিডিবিএফ কাজ করে যাচ্ছে। এর লক্ষ্যঃ</p>\n" +
              "\n" +
              "<ul>\n" +
              "\t<li>গ্রামের প্রতিটি বাড়িকে অর্থনৈতিক কার্যক্রমের কেন্দ্রবিন্দুতে এবং প্রতিটি গ্রাম সংগঠনকে এক একটি অর্থনৈতিক ইউনিট হিসেবে গড়ে তোলার মাধ্যমে গ্রামের সামগ্রিক উন্নয়ন সাধন</li>\n" +
              "\t<li>দক্ষতা বৃদ্ধির মাধ্যমে গ্রামাঞ্চলের মানব সম্পদ উন্নয়ন এবং উৎপাদনমুখী কার্যক্রমের সাথে তাদের সম্পৃক্তকরণ</li>\n" +
              "\t<li>স্ব-কর্মসংস্থান ও আয় বৃদ্ধিমূলক কার্যক্রমের মাধ্যমে পারিবারিক আয় বৃদ্ধি, তথা দারিদ্র বিমোচন</li>\n" +
              "\t<li>গ্রামীণ অর্থনীতিতে গতিশীলতা আনয়ন এবং স্থানীয় ভিত্তিতে সম্পদের সহজলভ্যতা নিরূপণ</li>\n" +
              "\t<li>গ্রামে বসবাসকারী নারী-পুরুষ নির্বিশেষে সকলকে অর্থনৈতিক এবং সামাজিক কার্যক্রমের সাথে সম্পৃক্তকরণ</li>\n" +
              "\t<li>গ্রামীণ জনগোষ্ঠির জীবন-যাত্রার মানোন্নয়ন</li>\n" +
              "\t<li>উৎপাদন বৃদ্ধির নিশ্চয়তা বিধানের জন্য আধুনিক ও বিজ্ঞান-সম্মত কৃষি পদ্ধতি শিক্ষা দেওয়া এবং কৃষির যাবতীয় উপকরণ যথাসময়ে সংগ্রহ ও বিলিবণ্টন</li>\n" +
              "\t<li>মিতব্যয়িতা ও সঞ্চয়ের মাধ্যমে যৌথ পুঁজি সৃষ্টিকরণ</li>\n" +
              "\t<li>গ্রামের দরিদ্র লোকজনকে ঋণের বেড়াজাল হতে বের করে পল্লী প্রভিডেন্ট ফান্ডের মাধ্যমে তহবিল গঠনে সহায়তা করা</li>\n" +
              "\t<li>পরিবেশ উন্নয়নকল্পে বৃক্ষরোপণ,</li>\n" +
              "\t<li>সেনিটেশনসহ নানামুখী সম্প্রসারণমূলক কার্যক্রম পরিচালনা</li>\n" +
              "</ul>\n</body></html>");
 	  db.insert("PDBF", null, values1);
 	  
 	  ContentValues values2=new ContentValues();
 	 values2.put("APPCODE", "ProkolpoInformation");
	  values2.put("CONTENT","<html><body><p><strong>একটি বাড়ি একটি খামার</strong></p>\n" +
              "\n" +
              "<p>বাংলার কৃষি এবং বাংলার কৃষক একই সুতোয় বাঁধা। যে কৃষক ধান ফলায়, সে বাড়ির আঙ্গিনায় শাক সবজির আবাদ করে, সে আবার তার পুকুরে মাছের চাষও করে। কেননা দৈনন্দিন জীবনে নিত্য প্রয়োজনীয় জিনিস পত্রের মধ্যে এগুলো আবশ্যকীয়ভাবে প্রয়োজন পড়ে। সুতরাং খামার ভিত্তিক পরিকল্পনা কৃষকের চলমান বা আবহমান বাস্তবতার ওপর নির্ভর করে। যেন একজন কৃষক-কৃষাণী তার চৌহদি থেকে নিত্য প্রয়োজনীয় সব কিছু অনায়াসে পেয়ে যায় এবং বাড়তি অংশ বাজারে বিকিয়ে অতিরিক্ত দু&#39;পয়সা আয় করতে পারেন। এদেশে প্রতিটি পরিবার যদি সমৃদ্ধ হয়ে সুখে থাকে তাহলে নিশ্চিত সুখে থাকবে বাংলাদেশ।</p>\n" +
              "\n" +
              "<p>&nbsp;</p>\n" +
              "\n" +
              "<p><strong>আশ্রায়ন</strong></p>\n" +
              "\n" +
              "<p>&nbsp;</p>\n" +
              "\n" +
              "<p>No related information found</p>\n" +
              "\n" +
              "<p>&nbsp;</p>\n" +
              "\n" +
              "<p><strong>গুচ্ছগ্রাম ও আদর্শগ্রাম</strong></p>\n" +
              "\n" +
              "<p>&nbsp;</p>\n" +
              "\n" +
              "<p>No related information found</p>\n</body></html>");
	  db.insert("PDBF", null, values2);

        ContentValues values3=new ContentValues();
        values3.put("APPCODE", "KormokortaInformation");
        values3.put("CONTENT","<html><body><div style=\"text-align: center\">\n" +
                "<p><strong>সম্পাদক</strong></p>\n" +
                "\n" +
                "<p>এম. এ কাদের সরকার</p>\n" +
                "\n" +
                "<p>পল্লী উন্নয়ন ও সমবায় বিভাগ, এলজিআরডি ও সি চেয়ারপারসন, বিওজি, পিডিবিএফ</p>\n" +
                "</div>\n" +
                "\n" +
                "<p>&nbsp;</p>\n" +
                "\n" +
                "<div style=\"text-align: center\">\n" +
                "<p><strong>পরিচালন অধিকর্তা</strong></p>\n" +
                "\n" +
                "<p>মো. মাহবুবুর রহমান</p>\n" +
                "\n" +
                "<p>পল্লী দারিদ্র্য বিমোচন ফাউন্ডেশন</p>\n" +
                "</div>\n" +
                "\n" +
                "<p>&nbsp;</p>\n</body></html>");
        db.insert("PDBF", null, values3);

        ContentValues values4=new ContentValues();
        values4.put("APPCODE", "SebaInformation");
        values4.put("CONTENT","<html><body><p><strong>ক্ষুদ্র ঋণ </strong></p>\n" +
                "\n" +
                "<p><strong><strong>সোলার প্রকল্প </strong></strong></p>\n" +
                "\n" +
                "<p><strong><strong><strong>ছোট এন্টারপ্রাইজ </strong></strong></strong></p>\n" +
                "\n" +
                "<p><strong><strong><strong><strong>সঞ্চয় </strong></strong></strong></strong></p>\n" +
                "\n" +
                "<p style=\"text-align:center\"><strong><strong><strong>সাধারণ</strong></strong></strong></p>\n" +
                "\n" +
                "<p style=\"text-align:center\"><strong><strong><strong>সোনালি</strong></strong></strong></p>\n" +
                "\n" +
                "<p style=\"text-align:center\"><strong><strong><strong>ফিক্সড</strong></strong></strong></p>\n</body></html>");
        db.insert("PDBF", null, values4);

        ContentValues values5=new ContentValues();
        values5.put("APPCODE", "JogajogInformation");
        values5.put("CONTENT","<html><body><div style=\"text-align: center\">\n" +
                "<p><strong>কেন্দ্রস্থান</strong></p>\n" +
                "\n" +
                "<p>পল্লী দারিদ্র্য বিমোচন ফাউন্ডেশন</p>\n" +
                "\n" +
                "<p>বাড়ি # ৫, রোড # ৩, হাজী রোড, রূপনগর,</p>\n" +
                "\n" +
                "<p>মিরপুর-২,</p>\n" +
                "\n" +
                "<p>ঢাকা-১২১৬</p>\n" +
                "\n" +
                "<p>বাংলাদেশ</p>\n" +
                "&nbsp;\n" +
                "\n" +
                "<p><strong>যোগাযোগ</strong></p>\n" +
                "\n" +
                "<p>ফোন: + 880-2-8032925-6, 8032936.</p>\n" +
                "\n" +
                "<p>ফ্যাক্স: + 880-2-8031597</p>\n" +
                "\n" +
                "<p>ইমেইল: info@pdbf.gov.bd</p>\n" +
                "</div>\n</body></html>");
        db.insert("PDBF", null, values5);

        ContentValues values6=new ContentValues();
        values6.put("APPCODE", "GoogleMapInformation");
        values6.put("CONTENT","<html><body><p>hello from all Google map</p></body></html>");
        db.insert("PDBF", null, values6);

        ContentValues values7=new ContentValues();
        values7.put("APPCODE", "OvijogInformation");
        values7.put("CONTENT","<html><body><div style=\"text-align: center\">\n" +
                "<p>ফোন: + 880-2-8032925-6, 8032936.</p>\n" +
                "\n" +
                "<p>ফ্যাক্স: + 880-2-8031597</p>\n" +
                "\n" +
                "<p>ইমেইল: info@pdbf.gov.bd</p>\n" +
                "</div>\n</body></html>");
        db.insert("PDBF", null, values7);

        ContentValues values8=new ContentValues();
        values8.put("APPCODE", "DownloadAbleFormInformation");
        values8.put("CONTENT","<html><body><p>hello from all Download</p></body></html>");
        db.insert("PDBF", null, values8);

		Log.w("DATABASE: ", db.getPath());
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + "PDBFS");
		
		onCreate(db);
		
	}

}
