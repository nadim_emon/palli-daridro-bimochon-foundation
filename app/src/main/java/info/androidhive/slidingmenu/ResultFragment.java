package info.androidhive.slidingmenu;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import info.androidhive.slidingmenu.MainActivity;
import info.androidhive.slidingmenu.R;

public class ResultFragment extends Fragment {

    TextView height;
    TextView weight;
    TextView age;
    TextView bmi;
    TextView gender;
    TextView perfectWeight;
    TextView perfectBMI;
    TextView under, normal, over, obe;

	public ResultFragment(){}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_result, container, false);

        return rootView;
    }
}
